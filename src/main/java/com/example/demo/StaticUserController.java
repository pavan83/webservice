package com.example.demo;

import com.example.demo.exception.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
public class StaticUserController {


    @Autowired
    StaticUserService userService;

    @Autowired
    MessageSource messageSource;

    @GetMapping(value = "/getData")
    public List<User> getUserData() {
        return userService.getAllUserData();
    }

    @GetMapping(value = "/getById/{id}", produces = {"application/json", "application/xml"}, consumes = {"application/json", "application/xml"})
    public User getUserData(@PathVariable("id") String id) {
        User user = userService.findById(id);
        if (user == null) {
            throw new UserNotFoundException("Id :" + id);
        }
        return user;
    }

    @GetMapping(value = "/deleteById/{id}")
    public String gdeleteByIdUser(@PathVariable("id") String id) {
        return userService.deleteUser(id);
    }

    //validation and post data
    @PostMapping(value = "/post")
    public ResponseEntity<List<String>> gdeleteByIdUser(@Valid @RequestBody User user) {
        userService.addUser(user);


        return new ResponseEntity<List<String>>(HttpStatus.OK);
    }


    // internalization with externlize msg
    @GetMapping(value = "/get")
    public String show() {
        return messageSource.getMessage("good.moring", null, LocaleContextHolder.getLocale());

    }

    // send data from header single

    @GetMapping(value = "/header")
    public String DataFromHeader(@RequestHeader("key") String language) {
        return language;
    }

    // send data from header list

    @GetMapping(value = "/listHeader")
    public Map<String, String> listHeader(@RequestHeader Map<String, String> headers) {
        return headers;
    }


    @GetMapping(value = "/beckheader")
    public Object frombeacen() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("pava","data");
        return headers;
    }


}

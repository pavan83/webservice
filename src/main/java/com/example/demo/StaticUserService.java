package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class StaticUserService {

	static List<User> userlist = new ArrayList<User>();
	private static int count = 4;

	static {
		userlist.add(new User("1", "pavan", "7509960574"));
		userlist.add(new User("2", "vaishali", "7509960574"));
		userlist.add(new User("3", "shubham", "7509960574"));

	}

	public List<User> getAllUserData() {
		return userlist;
	}


	public User findById(String id) {
		return userlist.stream()
				.filter(customer -> id.equals(customer.getId()))
				.findAny()
				.orElse(null);

	}

	public String deleteUser(String id) {
		for (User u : userlist) {
			if (u.getId().equals(id)) {
				userlist.remove(u);

				return "deleted";
			}
			break;
		}
		return "notfoudn";
	}

	public User addUser(User user) {
		if (user.getId() == null) {
			user.setId(String.valueOf(count++));
			userlist.add(user);
			return user;
		} else {
			for (User users : userlist) {
				if (users.getId().equals(user.getId())) {
					users.setName(user.getName());
					users.setNumber(user.getNumber());
					return  users ;
				}
			}
		}
		return  null;
	}

}

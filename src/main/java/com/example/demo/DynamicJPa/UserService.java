package com.example.demo.DynamicJPa;

import com.example.demo.User;
import com.example.demo.exception.UserNotFoundException;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;


	static List<User> userlist = new ArrayList<User>();
	private static int count = 4;

	static {
		userlist.add(new User("1", "pavan", "7509960574"));
		userlist.add(new User("2", "vaishali", "7509960574"));
		userlist.add(new User("3", "shubham", "7509960574"));

	}

	public List<User> getAllUserData() {
		return userRepository.findAll();
	}


	public Optional<User> findById(String id) {
		return userRepository.findById(id);

	}

	public void deleteUser(String id) {
		 userRepository.deleteById(id);
	}

	public User addUser(User user) {
		 userExist(user.getName());
		 if(userExist(user.getName()) !=null & user.getId() == null )
		 {
            throw  new UserNotFoundException("alreday exist");
		 }
		if(user.getId() == null )
		{
			return	userRepository.save(user);
		}else {
			Optional<User> useInfp = userRepository.findById(user.getId());
			User u = new User();
			u.setId(useInfp.get().getId());
			u.setNumber(useInfp.get().getNumber());
			u.setName(useInfp.get().getNumber());
			return  user;

		}
	}

	private User userExist(String name) {
		return userRepository.findByName(name);

	}

	public List<User> getUserDataBySearch(User user) {
	return 	userRepository.findUsers(user);
	}
}

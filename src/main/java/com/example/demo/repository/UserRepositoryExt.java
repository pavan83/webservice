package com.example.demo.repository;

import com.example.demo.User;

import java.util.List;

public interface UserRepositoryExt {
    List<User> findUsers(User userDto);


}

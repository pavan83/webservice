package com.example.demo.repository;

import com.example.demo.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sound.midi.Soundbank;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRepositoryExtImpl implements UserRepositoryExt {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<User> findUsers(User userDto) {
        Query query = constructQueryForUSers(userDto);
        List<User> userList = new ArrayList<>();
        userList = query.getResultList();
        return userList;
    }

    private Query constructQueryForUSers(User userDto) {

        String hql = "";

            hql += "select distinct u ";
            hql += " from User u where ";
        if (userDto.getName()!= null) {
            hql += " upper(u.name) = :name";
        }
        //excect search
        if (userDto.getQuickSearch() != null && userDto.getQuickSearch().trim().length() > 0) {
            hql += "  ( upper(u.name) like :quickSearch or upper(u.number) like :quickSearch )";
        }

        //sorting

        if (userDto.getSortOrder() != null  && userDto.getSortOrder().toUpperCase().equals("NAME")) {
            if (userDto.getSortOrder().length() > 0) {
                System.out.println("*********"+userDto.getSortOrder());

                hql += " order by " + userDto.getSortOrder() + " asc";
            } else {
                hql += " order by u.name desc";
            }
        }
//create query and set parameter
        final Query query = em.createQuery(hql.toString());
        if (userDto.getName() != null) {
            query.setParameter("name", userDto.getName());
        }
        // like search
        if (userDto.getQuickSearch().length() > 0) {
            query.setParameter("quickSearch", "%" + userDto.getQuickSearch().toUpperCase() + "%");
        }

//pagination
//        query.setFirstResult((userDto.getPageNumber() * userDto.getPageSize()) - userDto.getPageSize());
//        query.setMaxResults(userDto.getPageSize());
        query.setFirstResult(4);   //start from 0 index in datapase

        query.setMaxResults(8); // 2 result hi jaenge

        return  query;
    }
}

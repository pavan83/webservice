package com.example.demo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
@Entity
@Table(name ="USER_INFO")
public class User {

@Id
@GeneratedValue(generator = "UUID")
@GenericGenerator(
		name = "UUID",
		strategy = "org.hibernate.id.UUIDGenerator"
)@Column(name = "user_id")
private  String id;



@Size(min =2,message = "name should have 2 charecter")
@Column(name="user_name")
private String name;

@Column(name="user_number")
private String number;



	@Transient
	private String quickSearch;

	@Transient
	private String sortOrder;

public User(String id, String name, String number) {
	super();
	this.id = id;
	this.name = name;
	this.number = number;
}

	public User() {
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getQuickSearch() {
		return quickSearch;
	}

	public void setQuickSearch(String quickSearch) {
		this.quickSearch = quickSearch;
	}
}

package com.example.demo.imageUpload;

public class FileStorageException extends RuntimeException {
    public FileStorageException(String s) {
        super(s);
    }
}
